# czerolib

## Introduction

A library shared by programs made by zero

It can be imported as a whole (czerolib.h) or just some parts (individual header files)

Currently, there is documentation directly on the header files, in the [docs](docs) folder and in the [wiki](https://gitlab.com/zerowhy/czerolib/-/wikis/home) on the GitLab!

There is also comments on the code of the library itself (the .c files), explaining how the code works.

## Contributing

Feel free to contribute code, fixes, ideas and whatever else comes to your mind!
