/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "./../confread.h"

/* process string and return a pointer to a key-value pair data type (kvpair_t *).
 * a key-value pair exists if in a line of text a '=' character exists.
 * the first found from left to right takes priority. everything else is treated as data.
 */
kvpair_t * process_conf_string(char * str) {
    short int equal_pos = -1; // -1 value is used later for a condition
    short int strsize = strlen(str); // strlen() counts every character excluding the terminating null byte ('\0').

    /* read every char of string to find '=' character */
    for (int i = 0; i < strsize; i++) {
        if (str[i] == '=') {
            equal_pos = i;
            break; // found '='
        }
    }

    if (equal_pos != -1) { // check if '=' was found.
        kvpair_t * kvp = malloc(sizeof(kvpair_t)); // allocate key-value pair data type, which is defined in the header file.
        check_malloc(kvp);

        /* read key */
        for (int i = 0; i < equal_pos; i++) { // read characters before the '=' character.
            kvp->key[i] = str[i]; // copy the characters
        }
        /* read value
         * separate counter for resulting value string.
         * this is important as the read line character position does not start with zero
         */
        int count = 0; //
        for (int i = equal_pos+1; i < strsize-1; i++) { // read characters after the '=' character.
            kvp->value[count] = str[i];
            count++;

        /* remove some white space. function from common.h */
        strip_spaces(kvp->key);
        strip_spaces(kvp->value);
        }
        return kvp; // return the resulting pointer
    } else {
        return NULL; // return null if no key-value pair is found
    }
}

/* initializes all of confread's essential variables.
 * this is useful as the programmer does not need to understand the underlying architecture,
 * creating an abstraction layer and resulting in making the C coder's life easier.
 *
 * this gets the config location from the filename variable.
*/
confread_t * init_confread(char filename[]) {
    confread_t * cr = malloc(sizeof(confread_t));
    check_malloc(cr);
    cr->filename = calloc(strlen(filename)+1, sizeof(char)); // +1 on strlen() to account for null byte.
    check_malloc(cr->filename);

    strcpy(cr->filename, filename); // use strcpy to copy strings.
    cr->kvpair_arr = NULL;
    cr->quantity = 0;
    cr->num_read = 0;

    return cr;
}

/* inserts a key-value pair into the array kvpair_t ** kvpair_arr located in confread_t data type.
 * for internal use of this lib, this is not made available to the programmer.
*/
void insert_kvpair(confread_t * cr, kvpair_t * kvp) {
    if (cr->quantity == 0) {
        /* initialization */
        cr->kvpair_arr = malloc(sizeof(kvpair_t *));
        check_malloc(cr->kvpair_arr); // checks for memory allocation fail (from common.h).
        cr->kvpair_arr[0] = kvp; // store first key-value pair.
    } else {
        /* dynamic memory allocation
         * the way this code works is by allocating more memory (enough for 1 more element), copying all of the data from the
         * old block of memory to the new one, adding the new key-value pair and finally releasing the old block of memory.
        */
        kvpair_t ** kvpair_arr_old_ptr = cr->kvpair_arr; // store pointer to the old block of memory.
        cr->kvpair_arr = calloc(cr->quantity+1, sizeof(kvpair_t)); // allocate memory to hold the old block + 1.
        // note: calloc() is not necessary, malloc() could have been used instead.
        check_malloc(cr->kvpair_arr);
        memcpy(cr->kvpair_arr, kvpair_arr_old_ptr, cr->quantity * sizeof(kvpair_t *)); // copy old block data to new one.
        free(kvpair_arr_old_ptr); // free old block of memory.
        cr->kvpair_arr[cr->quantity] = kvp; // add the new key-value pair.
    }

    cr->quantity++; // update key-value pair quantity.
}

/* mainly used inside a while loop to read values.
 * pretty simple to understand!
*/
kvpair_t * kvpair_iterator(confread_t * cr) {
    /* run only if there are still key-value pairs yet to be read */
    if (cr->num_read < cr->quantity) {
        kvpair_t * result = cr->kvpair_arr[cr->num_read]; // get the pointer to the key-value pair.
        cr->num_read++; // update number of read key-value pairs.
        return result;
    } else {
        cr->num_read = 0; // reset value so the iterator can be used again.
        return NULL; // returning null when the iterator ends is useful as it can be used inside a while loop.
    }
}

/* reads data from disk
 * writes and reads information from confread_t data type argument (confread_t * cr).
 */
void read_conf(confread_t * cr) {
    /* variable initialization */
    kvpair_t * kvp;
    FILE * conf_f;
    char * buff;

    conf_f = fopen(cr->filename, "r"); // open file for reading

    /* allocate memory for at most MAX_LINE_LENGTH characters of type char
     * this can be problematic as the file may have multi-byte characters (such as Japanese characters),
     * which would cause unexpected behavior (one character can occupy the same space as 4 characters).
     * a better way to implement this is with dynamic memory allocation.
    */
    buff = calloc(MAX_LINE_LENGTH, sizeof(char)); // calloc() for its initialization (writes '\0' on the allocated space).
    check_malloc(buff);

    /* reads every line of the file */
    while (fgets(buff, MAX_LINE_LENGTH, conf_f)) {
        strip_spaces(buff); // remove trailing and leading white space.
        if ((kvp = process_conf_string(buff)) != NULL) { // process string and check if any key-value pairs exists.
            insert_kvpair(cr, kvp); // insert into array.
        }
    }

    free(buff); // don't forget to free allocated memory!
    fclose(conf_f); // and close the file!
}

/* releases memory allocated during used during operation
 * this is really important to not have memory leaks!
 * a simple call to free all of the variables makes it easy for the programmers.
 * a good way I found to not have memory leaks is to trace every single memory allocation call until it gets free'd later.
 * as a bonus step, I recommend running a test run in a loop and observing the memory usage with a process viewer (like htop).
*/
void exit_confread(confread_t * cr) {
    /* a loop is necessary to free all key-value pairs, as they are allocated dynamically */
    for (int i = 0; i < cr->quantity; i++) {
        free(cr->kvpair_arr[i]);
    }
    free(cr->filename);
    free(cr->kvpair_arr);
    free(cr);
}
