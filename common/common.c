/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "./../common.h"

void check_malloc(void * ptr) {
    /* test pointer */
    if (ptr == NULL) {
        perror("check_malloc: Out of memory!"); // print to stderr.
        exit(1);
    }
}

void strip_spaces(char * str) {
    int len = strlen(str); // get length of string.
    char * new_str = calloc(len, sizeof(char)); // allocate memory for placeholder string.
    /* note: it is important to use calloc() because it initializes the allocated space with null characters ('\0') */

    check_malloc(new_str); // check if memory allocation succeeded.

    /* loop to copy every character except white space */
    int cnt = 0; // counter for placeholder string
    for (int i = 0; i < len; i++) { // loop through characters of existing string
        if (str[i] != ' ') {
            new_str[cnt] = str[i]; // copy character
            cnt += 1;
        }
    }

    memcpy(str, new_str, len); // copy the modified string (new_str) to the given pointer (void * ptr)
    free(new_str); // don't forget to free allocated memory!
}
