/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __CONFREAD_H__
#define __CONFREAD_H__

#include "common.h"
#include <stdbool.h>

#define MAX_LINE_LENGTH 255
#define MAX_KEY_SIZE 64
#define MAX_VALUE_SIZE 64

/* struct to store key-value pairs */
typedef struct kv_pair {
    char key[MAX_KEY_SIZE];
    char value[MAX_VALUE_SIZE];
} kvpair_t;

/* struct to store information about confread's execution */
typedef struct confread {
    char * filename;
    kvpair_t ** kvpair_arr;
    int quantity;
    int num_read;
} confread_t;

/* init_confread(): initializes the library, allocating initial memory and setting some default values.
 * do not forget to call exit_confread() on the returning pointer after you are done using this library.
 *
 * char filename[]: filename of the file to read configuration from.
 *
 * return: this function returns a pointer to the confread data type, essential for all other functions of this library.
*/
confread_t * init_confread(char filename[]);

/* read_conf(): reads the file configured in init_confread() stored in the pointer returned by it.
 * the read key-value pairs are stored and can be read by other functions (such as kvpair_iterator())
 *
 * confread_t * cr: the confread_t pointer returned by init_confread().
 *
 * this function returns no value.
*/
void read_conf(confread_t * cr);

/* kvpair_iterator(): iterator to be used to get read key-value pairs.
 * this function is designed to work inside a while loop:
 *  confread_t * cr; kvpair_t * kvp;
 *  while ((kvp = kvpair_iterator(cr)) != NULL)
 *
 * after reading through all values, if the function is called again, it starts back on the beginning of the key-value pairs.
 *
 * confread_t * cr: the confread_t pointer returned by init_confread().
 *
 * return: this function returns a pointer to the kvpair_t data type.
 * this is a struct which holds a key (char key[]) and a value (char value[])
 * after reading all of the values stored in the config file read by read_conf(), this function returns NULL.
*/
kvpair_t * kvpair_iterator(confread_t * cr);

/* exit_confread(): exit the library and free up all allocated memory by it.
 * this should be called when you are done reading all the key-value pairs you need.
 *
 * confread_t * cr: the confread_t pointer returned by init_confread().
 *
 * this function returns no value.
*/
void exit_confread(confread_t * cr);

#endif
