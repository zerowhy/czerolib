# czerolib.h

## Introduction

This header file exist for the purpose of including everything that this library offers with one include header.

## Contents of this library

This library has currently 3 modules: `queue`, `confread` and `common`

The `common` module includes basic functions that are shared across all other modules.

The `queue` module includes a queue system.

The `confread` module includes a system to read key-value pairs from a file.

## How-to

To understand and use these libraries, I suggest reading their documentations, available in the `docs` folder. There is also documentation available directly on the header file, in case it is better for you.

## Contribute!

Feel free to suggest any changes or point to errors in the documentation!
