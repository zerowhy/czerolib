# confread.h

## Introduction

`confread.h` includes the headers for the functions available in `confread/confread.c`. These include functions to read a configuration file and get key-value pairs from it.

## Functions

### `confread_t * init_confread(char filename[])`

Initializes the library, allocating initial memory and setting some default values.

- Do not forget to call `exit_confread()` on the returning pointer after you are done using this library.
- `char filename[]`: filename of the file to read configuration from.
- Return: this function returns a pointer to the confread data type, essential for all other functions of this library.

### `void read_conf(confread_t * cr)`

Reads the file configured in `init_confread()` stored in the pointer returned by it.

- The read key-value pairs are stored and can be read by other functions (such as `kvpair_iterator()`)
- `confread_t * cr`: the `confread_t` pointer returned by `init_confread()`.
- This function returns no value.

### `kvpair_t * kvpair_iterator(confread_t * cr)`

Iterator to be used to get read key-value pairs.

- This function is designed to work inside a while loop (see example in the end)
- After reading through all values, if the function is called again, it starts back on the beginning of the key-value pairs.
- `confread_t * cr`: the `confread_t` pointer returned by `init_confread()`.
- Return: this function returns a pointer to the `kvpair_t` data type.
    - This is a struct which holds a key (`char key[]`) and a value (`char value[]`)
    - After reading all of the values stored in the config file read by `read_conf()`, this function returns NULL.

Example:
```c
confread_t * cr = init_confread("file.conf");
kvpair_t * kvp;
while ((kvp = kvpair_iterator(cr)) != NULL) {
    printf("%s=%s", kvp->key, kvp->value); // prints key-value pair to stdout
}
```

### `void exit_confread(confread_t * cr)`

Exit the library and free up all allocated memory by it.

- This should be called when you are done reading all the key-value pairs you need.
- `confread_t * cr`: the `confread_t` pointer returned by `init_confread()`.
- This function returns no value.
