# queue.h

## Introduction

`queue.h` includes the headers for the functions available in `queue/queue.c`. These include functions to create and use a queue system ([FIFO](https://en.wikipedia.org/wiki/FIFO_\(computing_and_electronics\))).

## Functions

### `queue * init_queue(int initial_size, int mem_alloc_step)`

Initializes the queue, setting some initial values and allocating initial memory.

- Do not forget to call `end_queue()` with the returning pointer.
- `int initial_size`: set the initial size (in number of elements) of the array that will store your data pointers. This should be a guess on how much you need.
  - It is better to underestimate as the queue system dynamically allocates memory.

- `int mem_alloc_step`: set how many elements you want allocate per memory allocation call. This should be set a value to balance CPU and memory usage.
  - The higher the value, the less CPU it consumes and the more memory it wastes.

- Return: this function returns a pointer to the queue data type, necessary for all other functions from this library.

### `void enqueue(queue * queue, void * data)`

Adds a value to the queue.

- `queue * queue`: the queue pointer returned by `init_queue()`.
- `void * data`: the pointer to the data that will be stored in the queue.
- This function returns no value.

### `void * dequeue(queue * queue)`

Returns the first element from the queue

- `queue * queue`: the queue pointer returned by `init_queue()`.
- Return: this function returns a pointer to the data if the queue has any.
- In case of a empty queue, it returns a null pointer.

### `void end_queue(queue * q)`

Terminates the queue, freeing memory.

- Note: this function does not call `free()` to the data pointers added with `enqueue()`.

- `queue * queue`: the queue pointer returned by `init_queue()`.

### `void show(queue * queue)`

Prints the queue's data as an integer to stdout.

- This is mainly used for debugging, as it is a very simple function.

- `queue * queue`: the queue pointer returned by `init_queue()`.

- This function returns no value.
