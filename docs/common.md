# common.h

## Introduction

`common.h` includes the headers for the functions available in `common/common.c`. These include some common functions that are universally useful in a lot of programs.

## Functions

### `check_malloc(void * ptr)`

Checks if memory allocation succeeded.

- This function works by testing if the pointer is null. On fail, a message is printed to `stderr` and `exit()` is called.
- `void * ptr`: pointer to be tested.
- This function returns no value.

### `void strip_spaces(char * str)`

Remove spaces on the start and on the end of the string.

- This function modifies the string directly, returning no value.
- `char * str`: string to be modified.
- This function returns no value.
