/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __CZEROLIB_QUEUE_H__
#define __CZEROLIB_QUEUE_H__

#include "common.h"

/* this struct stores all the information about the queue.
 * the values inside this struct should not have to be accessed at all by the programmers that use the library
*/
typedef struct queue {
    void ** values; // the array that stores the pointers to the data
    int num_values; // values stored in the queue
    int mem_allocated; // in bytes
    int mem_allocate_step; // in number of elements
} queue;

/* init_queue(): initializes the queue, setting some initial values and allocating initial memory.
 * do not forget to call end_queue() with the returning pointer.
 *
 * int initial_size: set the initial size (in number of elements) of the array that will store your data pointers.
 * this should be a guess on how much you need.
 * it is better to underestimate as the queue system dynamically allocates memory.
 *
 * int mem_alloc_step: set how many elements you want allocate per memory allocation call.
 * this should be set a value to balance CPU and memory usage.
 * the higher the value, the less CPU it consumes and the more memory it wastes.
 *
 * return: this function returns a pointer to the queue data type, necessary for all other functions from this library.
*/
queue * init_queue(int initial_size, int mem_alloc_step);

/* enqueue(): adds a value to the queue.
 *
 * queue * queue: the queue pointer returned by init_queue().
 *
 * void * data: the pointer to the data that will be stored in the queue.
 *
 * this function returns no value.
*/
void enqueue(queue * queue, void * data);

/* dequeue(): returns the first element from the queue
 *
 * queue * queue: the queue pointer returned by init_queue().
 *
 * return: this function returns a pointer to the data if the queue has any.
 * in case of a empty queue, it returns a null pointer.
*/
void * dequeue(queue * queue);

/* end_queue(): terminates the queue, freeing memory.
 * note: this function does not call free() to the data pointers added with enqueue().
 *
 * queue * queue: the queue pointer returned by init_queue().
*/
void end_queue(queue * q);

/* show(): prints the queue's data as an integer to stdout.
 * this is mainly used for debugging, as it is a very simple function.
 *
 * queue * queue: the queue pointer returned by init_queue().
 *
 * this function returns no value.
*/
void show(queue * queue);

#endif
