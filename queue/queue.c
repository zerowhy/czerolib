/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "./../queue.h"

/* function to initialize a struct and its variables
 * making a function to set some important values and returning a pointer to a struct is useful as you can
 * make your program thread-safe without much hassle. the returned pointer acts similarly to a class object in OOP.
 *
 * in this case, my reasoning in getting an initial array size and a memory allocation step is to leave to the programmer
 * the balance between CPU or memory usage.
 * this is because of the fact that more malloc() calls leads to more CPU usage, but results in less unused memory.
*/
queue * init_queue(int initial_size, int mem_allocate_step) {
    /* initialize memory for queue data type */
    queue * q = malloc(sizeof(queue));
    check_malloc(q); // check if memory allocation completed successfully (from common.h)

    /* set queue initial values */
    q->mem_allocate_step = mem_allocate_step; // step in number of elements
    q->num_values = 0; // in number of elements. important to initialize as int type start with garbage value
    q->values = malloc(initial_size * sizeof(void *)); // initial array size
    check_malloc(q->values);
    q->mem_allocated = initial_size * sizeof(void *); // in bytes

    return q;
}


/* function to add a void pointer to the queue (which is an array)
 * this is a special function as it allows the programmer to not worry about the number of values stored in the queue, as it
 * dynamically allocates more memory as needed.
 * this is possible due to tracking how much memory is allocated and how many elements are currently stored in the array.
 *
 * to achieve dynamic memory allocation, this code allocates a new chunk of memory, copies the old array contents to the new
 * array, calls free() on the old array and adds the new values to the newly allocated array.
*/
void enqueue(queue * q, void * data) {
    /* test if there is unused memory for the data pointer to get stored in */
    if (q->num_values < q->mem_allocated/sizeof(void *)) {
        q->values[q->num_values] = data; // store pointer
        q->num_values += 1; // increment counter
    } else {
        /* else block that allocates more memory dynamically */
        int new_size = q->mem_allocated/sizeof(void *) + q->mem_allocate_step; // calculate number of elements for new array
        void ** new_queue = malloc(new_size * sizeof(void *)); // allocate memory for new array
        check_malloc(new_queue); // check if memory allocation completed successfully (from common.h)

        /* loop to copy values of old array to newly allocated array, which will substitute the old one */
        for (int i = 0; i < q->mem_allocated/sizeof(void *); i++)
            new_queue[i] = q->values[i];

        void ** old_queue = q->values; // save the pointer of the old array so no memory leak occurs (with free())
        q->values = new_queue; // store new array pointer to the struct
        free(old_queue); // no memory leaks

        q->mem_allocated += q->mem_allocate_step*sizeof(void *); // update how much memory has been allocated so far
        q->values[q->num_values] = data; // store data pointer to new array
        q->num_values += 1; // update number of values
    }
}


/* function to dequeue values from the queue (in a FIFO manner, which means First In First Out)
 * the most intriguing step of this function is moving all of the array data one step to the left
 * this function is mainly used as an iterator (inside a loop)
 *
 * this can be improved/added on with dynamic memory unallocation, but I thought the tradeoffs were not really worth it.
 * however, this may be a good idea to add as an option to the programmer in the future.
*/
void * dequeue(queue * q) {
    /* check if queue is empty (if there are any values stored in the array)*/
    if (q->num_values > 0) {
        void * result = q->values[0]; // get the pointer stored in the array, so it can be returned later

        /* this loop moves every store value back by one value, just like people walk forward on a queue when one leaves */
        for (int i = 0; i < q->num_values-1; i++) {
            q->values[i] = q->values[i+1];
        }
        q->num_values -= 1; // update the number of values stored
        return result;
    } else {
        /* return NULL if there are no values available.
         * this is useful for programmers to create a loop with this function.
         * example (interpret_value is a mock function and null_ptr is of type (void *)):
         *     while ((void_ptr = dequeue(q)) != NULL) interpret_value(void_ptr);
         * this example loop automatically exits out when the queue runs out of values.
        */
        return NULL;
    }
}


/* function to free allocated memory from initialization and program execution.
 * this is important to include to make the life of the programmer less tedious,
 * making all the free() calls inside one function.
 *
 * when making this kinds of functions always try to trace back all of the possible memory allocation calls.
 * this is really important to prevent memory leaks!
*/
void end_queue(queue * q) {
    free(q->values);
    free(q);
}

/* function that prints out the data inside the queue as integers.
 * this was mainly used for testing and debugging this library while developing.
*/
void show(queue * q) {
    int * data;
    for (int i = 0; i < q->num_values; i++) {
        data = (int *) q->values[i];
        printf("%d: %d\n", i, *data);
    }
}

