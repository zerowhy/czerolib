/* czerolib
*    Copyright (C) 2022  Lucas Santos
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __CZEROLIB_COMMON_H__
#define __CZEROLIB_COMMON_H__

#define CZEROLIB_VERSION 20_07_2022-1

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/* check_malloc(): checks if memory allocation succeeded.
 * this function works by testing if the pointer is null.
 * on fail, a message is printed to stderr and exit() is called.
 *
 * void * ptr: pointer to be tested.
 *
 * this function returns no value.
*/
void check_malloc(void * ptr);

/* strip_spaces(): remove spaces on the start and on the end of the string.
 * this function modifies the string directly, returning no value.
 *
 * char * str: string to be modified.
 *
 * this function returns no value.
*/
void strip_spaces(char * str);

#endif
